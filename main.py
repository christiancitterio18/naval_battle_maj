#!/usr/bin/env python3
"""
Main component of the application
"""
from src.Client import Client
from src.ClientAI import ClientAi
from src.Commands import Commands
from src.Server import Server

comm: Commands = Commands()
print('Walcome to Naval Battle! \nHere\'s what you can do: \n')

for k, v in comm.help_command().items():
    print(k + '. ' + v)
print('\n')

while True:

    option: str = input('Select a option from above: ')

    if option.lower() == 'rules':
        print('Game Rules: \n')
        for k, v in comm.rules_command().items():
            print(k + ': ' + v)
        print('\n')

    elif option.lower() == 'play':

        mode = input("Enter the mode (Client, Server, AI): ")

        if mode.lower() == 'client':
            client = Client()
            print('\nGame Over!')

        if mode.lower() == 'server':
            server = Server()
            print('\nGame Over!')

        if mode.lower() == 'ai':
            ai = ClientAi()
            print('Game Over!\n')

    elif option.lower() == 'quit':
        print('See you soon!')
        break

    else:
        print('Not an option!')
