def check_if_ships_collides(ship_coord: tuple, coordinates_used: list) -> bool:
    """
    Checks if ships coordinates are colliding with each other
    :param ship_coord: tuple | ships coordinates
    :param coordinates_used: list | Already used coordinates
    :return: bool | result of the operation
    """
    for coord in ship_coord:
        if coord in coordinates_used:
            return True

    return False


def check_if_ship_is_diagonal(ships_coordinates: tuple) -> bool:
    """
    Checks if ships are put on a diagonal
    :param ships_coordinates: dict | the ships
    :return: bool | result of the operation
    """
    result: bool = True

    # checks for two-block ships
    if len(ships_coordinates) == 2:
        b1 = tuple(ships_coordinates[0])
        b2 = tuple(ships_coordinates[1])
        # checks in ex. if A<B and 1=1 or A=A and 1<2
        if not (b1[0] == b2[0] and (b1[1] < b2[1])) and not (b1[1] == b2[1] and (ord(b1[0]) < ord(b2[0]))):
            print(2)
            result = False

    # checks for three-block ships
    if len(ships_coordinates) == 3:
        b1 = tuple(ships_coordinates[0])
        b2 = tuple(ships_coordinates[1])
        b3 = tuple(ships_coordinates[2])
        # checks in ex. if A<B<C and 1=1=1 or A=A=A and 1<2<3
        if not (((b1[0] == b2[0]) and (b2[0] == b3[0])) and b1[1] < b2[1] < b3[1]) and not (
                ((b1[1] == b2[1]) and (b2[1] == b3[1])) and (ord(b1[0]) < ord(b2[0]) < ord(b3[0]))):
            print(3)
            result = False

    # checks for four-block ships
    if len(ships_coordinates) == 4:
        b1 = tuple(ships_coordinates[0])
        b2 = tuple(ships_coordinates[1])
        b3 = tuple(ships_coordinates[2])
        b4 = tuple(ships_coordinates[3])
        # checks in ex. if A<B<C<D and 1=1=1=1 or A=A=A=A and 1<2<3<4
        if (b1[0] == b2[0] and b2[0] == b3[0] and b3[0] == b4[0]) or \
                (b1[1] == b2[1] and b2[1] == b3[1] and b3[1] == b4[1]):
            if not (ord(b1[0]) < ord(b2[0]) < ord(b3[0]) < ord(b4[0])) and not (b1[1] < b2[1] < b3[1] < b4[1]):
                result = False

    return result


def check_if_hit(coordinates, ship_board) -> bool:
    """
    Returns if the shoot hits a ship or not
    :param coordinates: str | hit coordinates
    :param ship_board: dict | board to be checked
    :return: bool | result of the shoot
    """
    shoot: tuple = tuple(coordinates)
    if ship_board[shoot[0]][int(shoot[1])] == 'O':
        return True
    else:
        return False


def check_if_sunk(enemy_shoots: list, ship: tuple) -> bool:
    """
    Returns if the ship is sunken or not
    :param enemy_shoots: list | list of enemy_shoots
    :param ship: tuple | ship coordinates
    :return: bool | True if sunken False otherwise
    """
    is_sunken = 0
    for i in list(ship):
        if i in enemy_shoots:
            is_sunken += 1

    if is_sunken == len(ship):
        return True
    else:
        return False
