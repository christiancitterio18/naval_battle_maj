import json
import socket

from src import DataPreparation, GameSetup, GameTurn


class Client(object):
    PORT = 7070
    SERVER = '127.0.0.1'
    ADDR = (SERVER, PORT)

    HEADER = 64
    FORMAT = 'utf-8'
    BUFFER = 1024

    user_ships: dict = {}
    enemy_shoots: list = []
    my_shoots: list = []
    user_boards: dict = {}
    is_my_turn: bool = True

    def __init__(self):

        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as c:
            c.connect(self.ADDR)

            print('Set up your game boards...\n')
            self.user_ships = GameSetup.players_ships()
            self.user_boards['ships_board'], self.user_boards['shoot_board'] = GameSetup.create_boards(self.user_ships)

            print(c.recv(1024).decode(self.FORMAT))

            while True:
                self.print_boards()

                if self.is_my_turn:
                    # my turn
                    print('Your turn...')
                    shoot_data: dict = DataPreparation.prepare_command_data('shoot',
                                                                            coordinates=input('Enter shoot coordinates'
                                                                                              ' (A0 to J9): ')
                                                                            .upper())

                    c.send(json.dumps(shoot_data, indent=2).encode(self.FORMAT))

                    self.my_shoots.append(shoot_data['data']['coordinates'])

                    print('Shoot to coordinates: ', self.my_shoots[-1])

                    res_from_client: dict = json.loads(c.recv(self.BUFFER).decode(self.FORMAT))

                    print(res_from_client['result'] + '!')

                    if res_from_client['game_over']:
                        print('You win!')
                        GameTurn.update_shoot_board(self.client_boards['shoot_board'],
                                                    res_from_client['result'],
                                                    self.my_shoots[-1])
                        self.print_boards()
                        break
                    else:

                        print('Updating our boards...')
                        GameTurn.update_shoot_board(self.user_boards['shoot_board'],
                                                    res_from_client['result'],
                                                    self.my_shoots[-1])
                        self.is_my_turn = False

                        res: dict = {
                            'turn_over': True
                        }

                        c.send(json.dumps(res, indent=2).encode(self.FORMAT))

                        self.is_my_turn = False
                        print('Our turn ended...')

                else:
                    # enemy turn
                    print('Waiting for enemy to shoot...')

                    message_from_client: dict = json.loads(c.recv(self.BUFFER).decode(self.FORMAT))
                    res, self.user_boards['ships_board'] = GameTurn.shoot_command(
                        message_from_client['data']['coordinates'],
                        self.user_boards['ships_board'],
                        self.enemy_shoots,
                        self.user_ships)
                    self.enemy_shoots.append(message_from_client['data']['coordinates'])

                    print('Enemy shot at coordinates: ', self.enemy_shoots[-1])

                    c.send(json.dumps(res, indent=2).encode(self.FORMAT))

                    print('Enemy is updating boards...')

                    if res['game_over']:
                        print('We lose!')
                        self.print_boards()
                        break
                    else:
                        res_from_client: dict = json.loads(c.recv(self.BUFFER).decode(self.FORMAT))
                        if res_from_client['turn_over']:
                            self.is_my_turn = True
                            self.print_boards()
                            print('Enemy turn is over...')
                        else:
                            print('Waiting for answer from enemy...')

    def print_boards(self):

        print('\n-----------ships_board----------\n')

        for k, v in self.user_boards['ships_board'].items():
            print(k + ' ' + ''.join(v))

        print('\n-----------shoots_board----------\n')

        for k, v in self.user_boards['shoot_board'].items():
            print(k + ' ' + ''.join(v))

        print()
