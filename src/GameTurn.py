import random
from itertools import product

from src import BoardVerification


def shoot_command(
    coordinates: str, ships_board: dict, enemy_shoots: list, allied_ships: dict
):
    """
    Returns the shoot result to player
    :return: dict | shoot results
    """
    # można strzelac w to samo pole
    response: dict = {
        "result": "",
        "shooted_coordinates": coordinates,
        "game_over": False,
    }

    if BoardVerification.check_if_hit(coordinates, ships_board):
        ships_board = update_ship_board(ships_board, coordinates)
        if checks_if_end(ships_board):
            response["game_over"] = True
            response["result"] = "hit and sunk"
        else:
            for k, v in allied_ships.items():
                if k == '1':
                    response["result"] = "hit and sunk"
                else:
                    for i in v:
                        if (coordinates in i) and BoardVerification.check_if_sunk(
                            enemy_shoots, i
                        ):
                            print(BoardVerification.check_if_sunk(enemy_shoots, i))
                            response["result"] = "hit and sunk"
                            return response, ships_board
                        else:
                            if coordinates in i:
                                response["result"] = "hit"
                                return response, ships_board

    else:
        response["result"] = "miss"

    return response, ships_board


def checks_if_end(board) -> bool:
    """
    Returns True or False
    :return: bool | check if board have "O"
    """
    for i in board:
        if "O" in board[i]:
            return False
    return True


def ai_turn(coordinates_used: set):
    row = "ABCDEFGHIJ"
    column = "0123456789"
    possible_shoot_table: list = list(product(row, column))
    coordinates = [i for i in possible_shoot_table if ''.join(list(i)) not in coordinates_used]
    coord = random.choice(coordinates)

    return ''.join(list(coord))


def update_shoot_board(board: dict, result: str, coordinates: tuple) -> dict:
    """
    Puts a X or a O by result value
    :param board: dict | the board to update
    :param result: str | the result of the shot
    :param coordinates: list | the list of coordinates to update
    :return: dict | the updated board
    """
    shoot_coordinates_tup = tuple(coordinates)
    if "hit" in result:
        board[(shoot_coordinates_tup[0])][int(shoot_coordinates_tup[1])] = "X"
    else:
        board[(shoot_coordinates_tup[0])][int(shoot_coordinates_tup[1])] = "O"

    return board


def update_ship_board(board: dict, coordinates: tuple) -> dict:
    """
    Puts a X where the ship was hit
    :param board: dict | the board to update
    :param coordinates: list | teh coordinates to update
    :return: dict | the updated board
    """
    shoot_coordinates_tup = tuple(coordinates)
    board[(shoot_coordinates_tup[0])][int(shoot_coordinates_tup[1])] = "X"

    return board
